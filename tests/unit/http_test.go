package unittests

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
	"gitlab.com/fastocloud/gofastocloud_http/gofastocloud_http"
	"gitlab.com/fastocloud/gofastocloud_http/gofastocloud_http/public"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestUnmarshalJSON(t *testing.T) {
	esc := `Disney Classics And U`
	res := gofastocloud_http.QueryEscape(esc)
	assert.Equal(t, res, `Disney%20Classics%20And%20U`)

	var testJSON []byte

	//  test StartStreamResponse
	var start public.StartStreamResponse
	testJSON = []byte(`{"data":null}`)
	err := json.Unmarshal(testJSON, &start)
	assert.True(t, err == nil)
	assert.Equal(t, start, public.StartStreamResponse{})

	//  test VersionResponse
	var version public.VersionResponse
	testJSON = []byte(`{"data":{"version":{"short":"1.23.4", "human_readable":"1.23.4 Revision: aa1f615c", "version": 1234}}}`)
	err = json.Unmarshal(testJSON, &version)
	assert.True(t, err == nil)
	assert.Equal(t, version, public.VersionResponse{Data: public.Version{Version: gofastogt.ProjectVersion{Short: "1.23.4", Version: 1234, HumanReadable: "1.23.4 Revision: aa1f615c"}}})

	// test StopStreamRequest
	var stop public.StopStreamRequest
	testJSON = []byte(`{"id":"id","force":true}`)
	err = json.Unmarshal(testJSON, &stop)
	assert.True(t, err == nil)
	assert.Equal(t, stop.Id, media.StreamId("id"))
	assert.Equal(t, stop.Force, true)

	testCases := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{force":true}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"","force":true}`),
		},
		{
			name:      "nil force",
			testArray: []byte(`{"id":"id"}`),
		},
	}

	for _, obj := range testCases {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &stop))
		})
	}

	//  ScanFolderRequest
	scan := public.ScanFolderRequest{}
	testJSON = []byte(`{"directory":"directory","extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &scan)
	assert.True(t, err == nil)
	assert.Equal(t, scan.Directory, "directory")
	assert.Equal(t, scan.Extensions, []string{"ext1", "ext2"})

	//  test directory by nil
	testJSON = []byte(`{"extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &scan)
	assert.Error(t, err)

	//  test directory by empty
	testJSON = []byte(`{"directory":"","extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &scan)
	assert.Error(t, err)

	//  test extensions by nil
	testJSON = []byte(`{"directory":"directory"}`)
	err = json.Unmarshal(testJSON, &scan)
	assert.Error(t, err)

	testJSON = []byte(`{"directory":"directory", "extensions":["",""]}`)
	err = json.Unmarshal(testJSON, &scan)
	assert.Error(t, err)

	// type ProbeRequestIn
	testVariable3 := public.ProbeRequestIn{}
	testJSON = []byte(`{"url":{"id":23,"uri":"wdwd"}}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	assert.NoError(t, err)
	assert.Equal(t, testVariable3.Url, media.InputUri{InputUrl: media.InputUrl{Id: 23, Uri: "wdwd"}})

	testCases3 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil url",
			testArray: []byte(`{"udwrl":{"id":23,"uri":"wdwd"}}`),
		},
	}

	for _, obj := range testCases3 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable3))
		})
	}

	// type ProbeRequestOut
	testVariable4 := public.ProbeRequestOut{}
	testJSON = []byte(`{"url":{"id":23,"uri":"wdwd"}}`)
	err = json.Unmarshal(testJSON, &testVariable4)
	assert.NoError(t, err)
	out := media.OutputUrl{Id: 23, Uri: "wdwd"}
	assert.Equal(t, testVariable4.Url, media.OutputUri{OutputUrl: out, OutputUriData: media.OutputUriData{}})

	testCases4 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil url",
			testArray: []byte(`{"udwrl":{"id":23,"uri":"wdwd"}}`),
		},
	}

	for _, obj := range testCases4 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable4))
		})
	}

	// type ChangeInputStreamRequest
	testVariable5 := public.InjectMasterInputRequest{}
	testJSON = []byte(`{"id":"wd", "url":{"id":1, "uri":"wer"}}`)
	err = json.Unmarshal(testJSON, &testVariable5)
	assert.NoError(t, err)
	assert.Equal(t, testVariable5.Sid, media.StreamId("wd"))
	inp := media.InputUrl{Id: 1, Uri: "wer"}
	assert.Equal(t, testVariable5.Url, media.InputUri{InputUrl: inp, InputUriData: media.InputUriData{}})

	testCases5 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"url":{"id":1, "uri":"wer"}}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "url":{"id":1, "uri":"wer"}}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":"wd", "url":{"id":, "uri":""}}`),
		},
	}

	for _, obj := range testCases5 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable5))
		})
	}

	// type ChangeInputStreamRequest
	testVariable6 := public.ChangeInputStreamRequest{}
	testJSON = []byte(`{"id":"wd", "channel_id": 1}`)
	err = json.Unmarshal(testJSON, &testVariable6)
	assert.NoError(t, err)
	assert.Equal(t, testVariable6.Sid, media.StreamId("wd"))
	assert.Equal(t, testVariable6.ChannelId, 1)

	testCases6 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"channel_id": 1}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "channel_id": 1}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":"wd", "channel_id": }`),
		},
	}

	for _, obj := range testCases6 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable6))
		})
	}

	// type RemoveMasterInputRequest
	testVariable7 := public.RemoveMasterInputRequest{}
	testJSON = []byte(`{"id":"wdw2", "url": {"id":1,"uri":"wf"}}`)
	err = json.Unmarshal(testJSON, &testVariable7)
	assert.NoError(t, err)
	assert.Equal(t, testVariable7.Sid, media.StreamId("wdw2"))
	inpp := media.InputUrl{Id: 1, Uri: "wf"}
	assert.Equal(t, testVariable7.Url, media.InputUri{InputUrl: inpp, InputUriData: media.InputUriData{}})

	testCases7 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"url": {"id":1,"uri":"wf"}}`),
		},
		{
			name:      "invlaid id",
			testArray: []byte(`{"id":"", "url": {"id":1,"uri":"wf"}}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":"wdw2"}`),
		},
		{
			name:      "uri empty",
			testArray: []byte(`{"id":23, "uri": ""}`),
		},
	}

	for _, obj := range testCases7 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable7))
		})
	}

	// type OutputUrl
	testVariable8 := public.RemoveVideoRequest{}
	testJSON = []byte(`{"path":"wdwdw"}`)
	err = json.Unmarshal(testJSON, &testVariable8)
	assert.NoError(t, err)
	assert.Equal(t, testVariable8.Path, "wdwdw")

	testCases8 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"pgeath":"wdwdw"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"path":""}`),
		},
	}

	for _, obj := range testCases8 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable8))
		})
	}

	// type MountBucketRequest
	testVariable9 := public.MountBucketRequest{}
	testJSON = []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"keykey","secret":"ujkj92rj"}`)
	err = json.Unmarshal(testJSON, &testVariable9)
	assert.NoError(t, err)
	assert.Equal(t, testVariable9.Name, "vasya")
	assert.Equal(t, testVariable9.Path, "D:9wd1kw/wd")
	assert.Equal(t, testVariable9.Key, "keykey")
	assert.Equal(t, testVariable9.Secret, "ujkj92rj")

	testCases9 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil name",
			testArray: []byte(`{"path":"D:9wd1kw/wd","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "empty name",
			testArray: []byte(`{"name":"","path":"D:9wd1kw/wd","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "nil path",
			testArray: []byte(`{"name":"vasya","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "empty path",
			testArray: []byte(`{"name":"vasya","path":"","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "nil key",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","secret":"ujkj92rj"}`),
		},
		{
			name:      "empty key",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"","secret":"ujkj92rj"}`),
		},
		{
			name:      "nil secret",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"keykey",}`),
		},
		{
			name:      "empty secret",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"keykey","secret":""}`),
		},
	}

	for _, obj := range testCases9 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable9))
		})
	}

	// type UnmountBucketRequest
	testVariable10 := public.UnmountBucketRequest{}
	testJSON = []byte(`{"path":"C://wdqwd?wdw/qwf2"}`)
	err = json.Unmarshal(testJSON, &testVariable10)
	assert.NoError(t, err)
	assert.Equal(t, testVariable10.Path, "C://wdqwd?wdw/qwf2")

	testCases10 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil path",
			testArray: []byte(`{"uri": "qwer"}`),
		},
		{
			name:      "invlaid path",
			testArray: []byte(`{"id":-234, "uri": "qwer"}`),
		},
	}

	for _, obj := range testCases10 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable10))
		})
	}
}
