module gitlab.com/fastocloud/gofastocloud_http

// gorilla/websocket
go 1.23.0

require (
	github.com/gorilla/websocket v1.5.3
	github.com/stretchr/testify v1.10.0
	gitlab.com/fastocloud/gofastocloud v1.17.1
	gitlab.com/fastogt/gofastogt v1.12.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
