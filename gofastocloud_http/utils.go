package gofastocloud_http

import (
	"net/url"
	"strings"

	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
)

func (fasto *FastoCloud) deleteActiveStream(sid media.StreamId) {
	for i, id := range fasto.streams {
		if id == sid {
			fasto.streams = append(fasto.streams[:i], fasto.streams[i+1:]...)
			break
		}
	}
}

func (fasto *FastoCloud) addActiveStream(sid media.StreamId) {
	if fasto.Contains(sid) {
		return
	}
	fasto.streams = append(fasto.streams, sid)
}

func QueryEscape(s string) string {
	res := url.QueryEscape(s)
	res = strings.Replace(res, "+", "%20", -1)
	return res
}
