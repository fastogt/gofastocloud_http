package gofastocloud_http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/fastocloud/gofastocloud_http/gofastocloud_http/public"
)

func (fasto *FastoCloud) GetSeason(sid string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/seasons/%s", sid)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.SeasonResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetSeasonEpisodes(sid string) ([]json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/seasons/%s/episodes", sid)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.SeasonEpisodesResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return response.Data, nil
}

func (fasto *FastoCloud) PaginateSeasons(offset int, limit int, name *string, visible *bool) (*public.PaginateSeasons, error) {
	uri := fmt.Sprintf("/server/db/seasons?offset=%d&limit=%d", offset, limit)
	if name != nil && len(*name) != 0 {
		esc := QueryEscape(*name)
		uri += fmt.Sprintf("&name=%s", esc)
	}
	if visible != nil {
		uri += fmt.Sprintf("&visible=%t", *visible)
	}
	req, err := fasto.makeHttpGetRequest(uri)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.PaginateSeasonsResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}
