package public

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// Get hardware hash node

type GetNodeHardwareHashRequest struct {
	Algo gofastogt.AlgoType `json:"algo"`
	Host string             `json:"host"`
}

type NodeHardwareHash struct {
	Key string `json:"key"`
}

type NodeHardwareHashResponse struct {
	Data NodeHardwareHash `json:"data"`
}

func (hash *GetNodeHardwareHashRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Algo *gofastogt.AlgoType `json:"algo"`
		Host *string             `json:"host"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Algo == nil {
		return errors.New("algo field required")
	}
	if request.Host == nil {
		return errors.New("host field required")
	}

	hash.Algo = *request.Algo
	hash.Host = *request.Host
	return nil
}

// Get hardware hash

type HardwareHash struct {
	Key string `json:"key"`
}

type HardwareHashResponse struct {
	Data HardwareHash `json:"data"`
}

// License
type UpdateLicenseRequest struct {
	Key gofastogt.LicenseKey `json:"key"`
}

type UpdateLicense struct {
	Key gofastogt.LicenseKey `json:"key"`
}

type UpdateLicenseResponse struct {
	Data UpdateLicense `json:"data"`
}

func (license *UpdateLicense) UnmarshalJSON(data []byte) error {
	request := struct {
		Key *gofastogt.LicenseKey `json:"key"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Key == nil {
		return errors.New("key field required")
	}

	license.Key = *request.Key

	return nil
}

// Upload file

type UploadFile struct {
	Info media.MediaUrlInfo `json:"info"`
	Path string             `json:"path"`
}

type UploadFileResponse struct {
	Data UploadFile `json:"data"`
}

// Start stream

type StartStream struct {
}

type StartStreamResponse struct {
	Data StartStream `json:"data"`
}

// Stop stream

type StopStreamRequest struct {
	Id    media.StreamId `json:"id"`
	Force bool           `json:"force"`
}

type StopStream struct {
}

type StopStreamResponse struct {
	Data StopStream `json:"data"`
}

func (stop *StopStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Id    *media.StreamId `json:"id"`
		Force *bool           `json:"force"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Id == nil {
		return errors.New("id field required")
	}

	if len(*request.Id) == 0 {
		return errors.New("id can't be empty")
	}

	if request.Force == nil {
		return errors.New("force field required")
	}
	stop.Id = *request.Id
	stop.Force = *request.Force
	return nil
}

// Restart stream

type RestartStreamRequest struct {
	Id media.StreamId `json:"id"`
}

type RestartStream struct {
}

type RestartStreamResponse struct {
	Data RestartStream `json:"data"`
}

func (restart *RestartStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Id *media.StreamId `json:"id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Id == nil {
		return errors.New("id field required")
	}
	if len(*request.Id) == 0 {
		return errors.New("id can't be empty")
	}
	restart.Id = *request.Id
	return nil
}

// Clean stream

type CleanStream struct {
}

type CleanStreamResponse struct {
	Data CleanStream `json:"data"`
}

// Version service

type Version struct {
	Version gofastogt.ProjectVersion `json:"version"`
}

type VersionResponse struct {
	Data Version `json:"data"`
}

// DB stats

type DBStats struct {
	Live     CountAndVersion `json:"streams"`
	Catchups CountAndVersion `json:"catchups"`
	Vods     CountAndVersion `json:"vods"`
	Episodes CountAndVersion `json:"episodes"`
	Seasons  CountAndVersion `json:"seasons"`
	Serials  CountAndVersion `json:"serials"`
}

type DBStatsResponse struct {
	Data DBStats `json:"data"`
}

type DBLiveStatsResponse struct {
	Data CountAndVersion `json:"data"`
}

type DBCatchupsStatsResponse struct {
	Data CountAndVersion `json:"data"`
}

type DBVodsStatsResponse struct {
	Data CountAndVersion `json:"data"`
}

type DBEpisodesStatsResponse struct {
	Data CountAndVersion `json:"data"`
}

type DBSeasonsStatsResponse struct {
	Data CountAndVersion `json:"data"`
}

type DBSerialsStatsResponse struct {
	Data CountAndVersion `json:"data"`
}

// Content

type Content struct {
	Lives        []json.RawMessage     `json:"streams"`
	LivesVersion gofastogt.UtcTimeMsec `json:"streams_version"`

	Catchups        []json.RawMessage     `json:"catchups"`
	CatchupsVersion gofastogt.UtcTimeMsec `json:"catchups_version"`

	Vods        []json.RawMessage     `json:"vods"`
	VodsVersion gofastogt.UtcTimeMsec `json:"vods_version"`

	Episodes        []json.RawMessage     `json:"episodes"`
	EpisodesVersion gofastogt.UtcTimeMsec `json:"episodes_version"`

	Seasons        []json.RawMessage     `json:"seasons"`
	SeasonsVersion gofastogt.UtcTimeMsec `json:"seasons_version"`

	Serials        []json.RawMessage     `json:"serials"`
	SerialsVersion gofastogt.UtcTimeMsec `json:"serials_version"`
}

type ContentResponse struct {
	Data Content `json:"data"`
}

type OttContent struct {
	Streams        []json.RawMessage     `json:"streams"`
	StreamsVersion gofastogt.UtcTimeMsec `json:"streams_version"`

	Vods        []json.RawMessage     `json:"vods"`
	VodsVersion gofastogt.UtcTimeMsec `json:"vods_version"`

	Serials        []json.RawMessage     `json:"serials"`
	SerialsVersion gofastogt.UtcTimeMsec `json:"serials_version"`
}

type OttContentResponse struct {
	Data OttContent `json:"data"`
}

// Service stats

type FullStatService struct {
	media.ServiceStatisticInfo

	gofastogt.LicensedProjectInfo
	OS gofastogt.OperationSystem `json:"os"`

	// virtual
	VSystem string `json:"vsystem"`
	VRole   string `json:"vrole"`
}

func NewFullStatService(stat media.ServiceStatisticInfo,
	lic gofastogt.LicensedProjectInfo, os gofastogt.OperationSystem, vsys string, vrole string) *FullStatService {
	return &FullStatService{stat, lic, os, vsys, vrole}
}

type StatServiceResponse struct {
	Data FullStatService `json:"data"`
}

// Probe
type ProbeRequestIn struct {
	Url media.InputUri `json:"url"`
}

type ProbeRequestOut struct {
	Url media.OutputUri `json:"url"`
}

type Probe struct {
	Probe media.ProbeData    `json:"probe"`
	Info  media.MediaUrlInfo `json:"info"`
}

type ProbeResponse struct {
	Data Probe `json:"data"`
}

func (requestIn *ProbeRequestIn) UnmarshalJSON(data []byte) error {
	request := struct {
		Url *media.InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Url == nil {
		return errors.New("url field required")
	}

	requestIn.Url = *request.Url
	return nil
}

func (requestOut *ProbeRequestOut) UnmarshalJSON(data []byte) error {
	request := struct {
		Url *media.OutputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Url == nil {
		return errors.New("url field required")
	}

	requestOut.Url = *request.Url
	return nil

}

// Scan folder

type ScanFolderRequest struct {
	Directory  string   `json:"directory"`
	Extensions []string `json:"extensions"`
}

type ScanFolder struct {
	Files []string `json:"files"`
}

type ScanFolderResponse struct {
	Data ScanFolder `json:"data"`
}

func (scan *ScanFolderRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Directory  *string   `json:"directory"`
		Extensions *[]string `json:"extensions"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Directory == nil {
		return errors.New("directory field required")
	}
	if len(*request.Directory) == 0 {
		return errors.New("directory can't be empty")
	}
	if request.Extensions == nil {
		return errors.New("extensions field required")
	}
	for _, str := range *request.Extensions {
		if len(str) == 0 {
			return errors.New("extensions can't be empty")
		}
	}

	scan.Directory = *request.Directory
	scan.Extensions = *request.Extensions
	return nil
}

// Stream statistic

type StreamStatistic struct {
	Stats *media.StreamStatisticInfo `json:"statistic"`
}

type StreamStatisticResponse struct {
	Data StreamStatistic `json:"data"`
}

// Streams config

type StreamConfig struct {
	Config json.RawMessage `json:"config"`
}

type StreamConfigResponse struct {
	Data StreamConfig `json:"data"`
}

// Streams statistics

type StreamsStatistic struct {
	Stats []media.StreamStatisticInfo `json:"statistics"`
}

type StreamsStatisticResponse struct {
	Data StreamsStatistic `json:"data"`
}

// Change input stream
type ChangeInputStreamRequest struct {
	Sid       media.StreamId `json:"id"`
	ChannelId int            `json:"channel_id"`
}

type ChangeInputStream struct {
}

type ChangeInputStreamResponse struct {
	Data ChangeInputStream `json:"data"`
}

func (change *ChangeInputStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid       *media.StreamId `json:"id"`
		ChannelId *int            `json:"channel_id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.ChannelId == nil {
		return errors.New("channel_id field required")
	}

	change.Sid = *request.Sid
	change.ChannelId = *request.ChannelId
	return nil
}

// Master sourse stream
type InjectMasterInputRequest struct {
	Sid media.StreamId `json:"id"`
	Url media.InputUri `json:"url"`
}

type InjectMasterInput struct {
}

type InjectMasterInputResponse struct {
	Data InjectMasterInput `json:"data"`
}

type RemoveMasterInputRequest struct {
	Sid media.StreamId `json:"id"`
	Url media.InputUri `json:"url"`
}

type RemoveMasterInput struct {
}

type RemoveMasterInputResponse struct {
	Data RemoveMasterInput `json:"data"`
}

func (injectMaster *InjectMasterInputRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid *media.StreamId `json:"id"`
		Url *media.InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.Url == nil {
		return errors.New("url field required")
	}
	injectMaster.Sid = *request.Sid
	injectMaster.Url = *request.Url
	return nil
}

func (removeMaster *RemoveMasterInputRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid *media.StreamId `json:"id"`
		Url *media.InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.Url == nil {
		return errors.New("url field required")
	}
	removeMaster.Sid = *request.Sid
	removeMaster.Url = *request.Url
	return nil
}

// Revome video

type RemoveVideoRequest struct {
	Path string `json:"path"`
}

type RemoveVideo struct {
}

type RemoveVideoResponce struct {
	Data RemoveVideo `json:"data"`
}

func (change *RemoveVideoRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Path *string `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}
	change.Path = *request.Path
	return nil
}

// Bucket
type MountBucketRequest struct {
	Name   string `json:"name"`
	Path   string `json:"path"`
	Key    string `json:"key"`
	Secret string `json:"secret"`
}

type UnmountBucketRequest struct {
	Path string `json:"path"`
}

type Bucket struct {
}

type BucketResponse struct {
	Data Bucket `json:"data"`
}

func (mountBucket *MountBucketRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Name   *string `json:"name"`
		Path   *string `json:"path"`
		Key    *string `json:"key"`
		Secret *string `json:"secret"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("name field required")
	}
	if len(*request.Name) == 0 {
		return errors.New("name can't be empty")
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}
	if request.Key == nil {
		return errors.New("key field required")
	}
	if len(*request.Key) == 0 {
		return errors.New("key can't be empty")
	}
	if request.Secret == nil {
		return errors.New("secret field required")
	}
	if len(*request.Secret) == 0 {
		return errors.New("secret can't be empty")
	}
	mountBucket.Name = *request.Name
	mountBucket.Path = *request.Path
	mountBucket.Key = *request.Key
	mountBucket.Secret = *request.Secret
	return nil
}

func (unmountBucket *UnmountBucketRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Path *string `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}
	unmountBucket.Path = *request.Path
	return nil
}

//Get Buckets

type Buckets struct {
	Path string `json:"path"`
	Name string `json:"name"`
}

type GetBucket struct {
	Bucket []Buckets `json:"data"`
}
type GetBucketResponse struct {
	Data GetBucket `json:"data"`
}

type CountAndVersion struct {
	Count   int                   `json:"count"`
	Version gofastogt.UtcTimeMsec `json:"version"`
}

type OffsetAndLimit struct {
	Limit  int `json:"limit"`
	Offset int `json:"offset"`
}

// Info

type Info struct {
	HlsHost  string `json:"hls_host"`
	VodsHost string `json:"vods_host"`
	CodsHost string `json:"cods_host"`
}

type InfoResponse struct {
	Data Info `json:"data"`
}

// Media info

type MediaInfo struct {
	HlsDir   string `json:"hls_directory"`
	VodsDir  string `json:"vods_directory"`
	CodsDir  string `json:"cods_directory"`
	ProxyDir string `json:"proxy_directory"`
}

type MediaInfoResponse struct {
	Data MediaInfo `json:"data"`
}

// Upload

type UploadSubtitles struct {
	Path string `json:"path"`
}

type UploadSubtitlesResponse struct {
	Data UploadSubtitles `json:"data"`
}

// Server statistics

type ServerStats struct {
	gofastogt.Machine

	gofastogt.LicensedProjectInfo
	OS gofastogt.OperationSystem `json:"os"`
}

type ServerStatsResponse struct {
	Data ServerStats `json:"data"`
}

// Snapshot

type Snapshot struct {
	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`
	gofastogt.Machine
}

type SnapshotResponse struct {
	gofastogt.IBytesConverter
	Data Snapshot `json:"data"`
}

// Snapshots

type Snapshots struct {
	Snapshots []Snapshot `json:"snapshots"`
}

type SnapshotsResponse struct {
	Data Snapshots `json:"data"`
}

func (s *Snapshot) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}
