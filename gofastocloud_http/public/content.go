package public

import (
	"encoding/json"

	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
)

type PaginateCatchups struct {
	CountAndVersion
	OffsetAndLimit
	Catchups []json.RawMessage `json:"catchups"`
}

type CatchupRequest struct {
	Programs []media.ProgramInfo
}

type CatchupResponse struct {
	Data json.RawMessage `json:"data"`
}

type PaginateCatchupsResponse struct {
	Data PaginateCatchups `json:"data"`
}

type PaginateEpisodes struct {
	CountAndVersion
	OffsetAndLimit
	Vods []json.RawMessage `json:"episodes"`
}

type EpisodeResponse struct {
	Data json.RawMessage `json:"data"`
}

type PaginateEpisodesResponse struct {
	Data PaginateEpisodes `json:"data"`
}

type PaginateLives struct {
	CountAndVersion
	OffsetAndLimit
	Streams []json.RawMessage `json:"streams"`
}

type LiveResponse struct {
	Data json.RawMessage `json:"data"`
}

type LiveCatchupsResponse struct {
	Data []json.RawMessage `json:"data"`
}

type PaginateLivesResponse struct {
	Data PaginateLives `json:"data"`
}

type PaginateSeasons struct {
	CountAndVersion
	OffsetAndLimit
	Seasons []json.RawMessage `json:"seasons"`
}

type SeasonResponse struct {
	Data json.RawMessage `json:"data"`
}

type SeasonEpisodesResponse struct {
	Data []json.RawMessage `json:"data"`
}

type PaginateSeasonsResponse struct {
	Data PaginateSeasons `json:"data"`
}

type PaginateSerials struct {
	CountAndVersion
	OffsetAndLimit
	Serials []json.RawMessage `json:"serials"`
}

type SerialResponse struct {
	Data json.RawMessage `json:"data"`
}

type SerialSeasonsResponse struct {
	Data []json.RawMessage `json:"data"`
}

type PaginateSerialsResponse struct {
	Data PaginateSerials `json:"data"`
}

type PaginateVods struct {
	CountAndVersion
	OffsetAndLimit
	Vods []json.RawMessage `json:"vods"`
}

type VodResponse struct {
	Data json.RawMessage `json:"data"`
}

type PaginateVodsResponse struct {
	Data PaginateVods `json:"data"`
}
