package gofastocloud_http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
	"gitlab.com/fastocloud/gofastocloud_http/gofastocloud_http/public"
)

func (fasto *FastoCloud) StartCatchup(cid media.StreamId) (*public.StartStream, error) {
	req, err := fasto.makeHttpGetRequest(fmt.Sprintf("/server/db/catchups/%s/start", cid))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetCatchup(cid media.StreamId) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/catchups/%s", cid)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.CatchupResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) PaginateCatchups(offset int, limit int, name *string, visible *bool, group *string) (*public.PaginateCatchups, error) {
	uri := fmt.Sprintf("/server/db/catchups?offset=%d&limit=%d", offset, limit)
	if name != nil && len(*name) != 0 {
		esc := QueryEscape(*name)
		uri += fmt.Sprintf("&name=%s", esc)
	}
	if visible != nil {
		uri += fmt.Sprintf("&visible=%t", *visible)
	}
	if group != nil && len(*group) != 0 {
		esc := QueryEscape(*group)
		uri += fmt.Sprintf("&group=%s", esc)
	}
	req, err := fasto.makeHttpGetRequest(uri)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.PaginateCatchupsResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) SyncCatchupsAndPrograms(programs []media.ProgramInfo) (*json.RawMessage, error) {
	var reqParams = public.CatchupRequest{
		Programs: programs,
	}
	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/catchup", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.CatchupResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}
